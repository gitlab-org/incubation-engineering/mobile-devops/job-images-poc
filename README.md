job-images-poc
==============

This project is a proof-of-concept for building macOS images for GitLab Runners. This should not be considered production code and is not supported currently. When ready for production, the code can be found at [https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/job-images](). 

The code is organized as follows:

* `ansible/` Ansible configuration and playbooks
* `packer/` macOS version specific Packer configuration and build scripts
* `tests/` tests ensuring expected configurations
* `toolchain/` job image configuration definitations

## How it works

This project uses [Packer](https://www.packer.io/), [Ansible](https://www.ansible.com/), and [Tart](https://tart.run/) to create a macOS machine image from an Apple [IPSW](https://en.wikipedia.org/wiki/IPSW) file. The generated machine image is optimized to be run as a GitLab runner that can be run as virtual machine in Tart, and connected to your projects on gitlab.com or your self-managed instance.

## What's included?

The inventory for each machine is described in the yaml files in the `toolchain` directory. Currently only macOS Ventura is included, but this will be expanded in the future.

* [toolchain/ventura.yml](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/job-images-poc/-/blob/main/toolchain/ventura.yml)

## Getting Started

### Prerequisites

To build and run these virtual machines, you'll need to have Tart and Packer installed on the host machine. They can both be installed via Homebrew:
 
```
brew install cirruslabs/cli/tart
brew install packer
```

You'll also need to install the required Packer plugins. This can be done by running:

```
packer init packer/templates/ventura-base.pkr.hcl
```

### IPSW and XIP Files

Apple uses IPSW and XIP files to distribute base installations for macOS and Xcode. These files are quite large and can be time consuming to download. It's recommended that you download the needed source files to the host machine to speed up the process. This also bypasses the need to provide login credentials to the App Store in order to install Xcode. [iPhone Wiki](https://www.theiphonewiki.com/wiki/Firmware) has links to all the macOS firmware versions, and use [xcodes](https://github.com/XcodesOrg/xcodes) to download Xcode XIP files (Apple ID login required).

The location of these files can be configured in the `packer/conf/{version}.pkrvars.hcl` files.

For example:

```
/opt/base_images/UniversalMac_12.6_21G115_Restore.ipsw
/opt/base_images/UniversalMac_13.4_22F66_Restore.ipsw
/opt/base_images/UniversalMac_14.0_23A5257q_Restore.ipsw
/opt/base_images/xcode/Xcode_13.4.1.xip
/opt/base_images/xcode/Xcode_14.3.1.xip
/opt/base_images/xcode/Xcode_15_beta.xip
```

## Build a Machine Image

1. Start by building a base image based on an IPSW supplied by Apple. This step involves walking through the standard onboarding flow of a new macOS machine. This process takes some time, so we create a snapshot once done and run the subsequent steps from the snapshot.
    
    ```
    $ packer build -var-file="packer/conf/ventura.pkrvars.hcl" packer/templates/ventura-base.pkr.hcl
    ```

2. Next, run the `toolchain` template to install all development tools. This will include Homebrew, Xcode, ruby, python, go, etc.

    ```
    $ packer build -var-file="packer/conf/ventura.pkrvars.hcl" packer/templates/toolchain.pkr.hcl
    ```
    
3. Finally, [register the gitlab-runner](https://docs.gitlab.com/runner/register/#macos) and configure it to start when the image boots. (more details coming soon)

## Testing

Tests are included to ensure the all provisioned software is installed and functioning correctly. Test can be run on the host machine against any built virtual machine. `sshpass` is required to run the tests and can be installed via Homebrew:

```sh
brew tap gitlab/shared-runners git@gitlab.com:gitlab-org/ci-cd/shared-runners/homebrew.git
brew install gitlab/shared-runners/sshpass
```

To run the tests, boot up the virtual machine with (`ventura-toolchain` is the image in this example):

```sh
tart run ventura-toolchain
```

Then run:

```sh
TARGET_HOST=$(tart ip ventura-toolchain) TARGET_PORT=22 LOGIN_PASSWORD=gitlab bundle exec rspec spec
```

## Starting the GitLab Runner

1. Start the virtual machine

    ```
    tart run ventura-toolchain
    ```
1. Create a new runner registration in your projects CI/CD settings

1. SSH to the virtual machine, and register the GitLab Runner

    ```
    ssh gitlab@$(tart ip ventura-toolchain) (password is gitlab)
    ```

    ```
    gitlab-runner register -n --url https://gitlab.com --executor shell --shell bash --token YOUR_TOKEN
    ```

1. Change the system shell to bash
    
    ```
    chsh -s /bin/bash
    ```

1. Start up the gitlab-runner

    ```
    gitlab-runner run
    ```
