packer {
  required_plugins {
    tart = {
      version = ">= 1.2.0"
      source  = "github.com/cirruslabs/tart"
    }
  }
}

variable "macos_version_name" {
  type =  string
}

variable "macos_version_number" {
  type =  string
}

source "tart-cli" "tart" {
  vm_base_name = "${var.macos_version_name}-base"
  vm_name      = "${var.macos_version_name}-toolchain"
  ssh_password = "gitlab"
  ssh_username = "gitlab"
  ssh_timeout  = "120s"
}

build {
  sources = ["source.tart-cli.tart"]

  provisioner "shell" {
    inline = [
      "mkdir ./provision",
    ]
  }

  provisioner "file" {
    destination = "./provision/ansible"
    source = "./ansible"
  }

  provisioner "file" {
    destination = "./provision/toolchain"
    source = "./toolchain"
  }

  provisioner "shell" {
    inline = [
      "ansible-playbook -v ./provision/ansible/playbooks/main.yml -i ./provision/ansible/inventory -e toolchain_version='${var.macos_version_name}'",
    ]
  }

  provisioner "shell" {
    inline = [
      "rm -rf ./provision",
    ]
  }
}