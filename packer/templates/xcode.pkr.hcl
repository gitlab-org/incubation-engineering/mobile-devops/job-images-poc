packer {
  required_plugins {
    tart = {
      version = ">= 1.2.0"
      source  = "github.com/cirruslabs/tart"
    }
  }
}

variable "macos_version_name" {
  type =  string
}

variable "xcode_source" {
  type = string
}

variable "xcode_version" {
  type = string
}

variable "xcode_major_version" {
  type = string
}

source "tart-cli" "tart" {
  vm_base_name = "${var.macos_version_name}-toolchain"
  vm_name      = "${var.macos_version_name}-xcode-${var.xcode_major_version}"
  ssh_password = "gitlab"
  ssh_username = "gitlab"
  ssh_timeout  = "120s"
}

build {
  sources = ["source.tart-cli.tart"]

  provisioner "shell" {
    inline = [
      "mkdir ./provision",
    ]
  }

  provisioner "file" {
    destination = "./provision/ansible"
    source = "./ansible"
  }

  provisioner "file" {
    destination = "./provision/xcode.xip"
    source = "${var.xcode_source}"
  }

  provisioner "shell" {
    inline = [
      "ansible-playbook -v ./provision/ansible/playbooks/xcode.yml -i ./provision/ansible/inventory -e xcode_version='${var.xcode_version}'"
    ]
  }

  provisioner "shell" {
    inline = [
      "rm -rf ./provision",
    ]
  }
}