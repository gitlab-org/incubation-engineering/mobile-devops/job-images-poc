# You can find macOS IPSW URLs on various websites like https://ipsw.me/
# and https://www.theiphonewiki.com/wiki/Beta_Firmware/Mac/13.x
// ipsw_source = "https://updates.cdn-apple.com/2022FallFCS/fullrestores/012-40537/0EC7C669-13E9-49FB-BD64-9EECC1D174B2/UniversalMac_12.6_21G115_Restore.ipsw"
ipsw_source = "/Volumes/base_images/UniversalMac_12.6_21G115_Restore.ipsw"
macos_version_name = "monterey"
macos_version_number = "12.6"
