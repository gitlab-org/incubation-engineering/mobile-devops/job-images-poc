# You can find macOS IPSW URLs on various websites like https://ipsw.me/
# and https://www.theiphonewiki.com/wiki/Beta_Firmware/Mac/13.x
// ipsw_source = "https://updates.cdn-apple.com/2023SpringFCS/fullrestores/032-84616/FD498BA4-F00E-4CC3-9E21-393DEC4D41B4/UniversalMac_13.4_22F2073_Restore.ipsw"
ipsw_source = "/opt/base_images/UniversalMac_13.4.1_22F82_Restore.ipsw"
macos_version_name = "ventura"
macos_version_number = "13.4.1"
