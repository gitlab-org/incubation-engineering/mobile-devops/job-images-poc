#!/bin/bash -e -o pipefail

echo 'Setting up ANSIBLE'

echo "Installing ansible..."
pip3 install ansible

touch ~/.zshrc
echo "export PATH=\"`python3 -m site --user-base`/bin:\$PATH\"" >> ~/.zshrc

touch ~/.zshenv
echo "export PATH=\"`python3 -m site --user-base`/bin:\$PATH\"" >> ~/.zshenv