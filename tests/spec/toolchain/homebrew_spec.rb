# frozen_string_literal: true

require 'spec_helper'

context 'homebrew' do
  brew_repository = machine_is_arm? ? '/opt/homebrew' : '/usr/local/Homebrew'

  # Install dir for homebrew should exist
  describe file("#{brew_repository}/.git") do
    it { should be_directory }
  end

  # Check that this actually runs homebrew
  describe command('brew help') do
    it { should be_a_successful_cmd }
    its(:stdout) { should match(/brew install FORMULA/) }
  end

  describe command('printenv') do
    it { should be_a_successful_cmd }

    its(:stdout) { should_not match(/^HOMEBREW_NO_AUTO_UPDATE=1$/) }
  end

  # Taps should be tapped
  context 'taps' do
    describe 'homebrew/homebrew-core' do
      let(:subject) { file("#{brew_repository}/Library/Taps/homebrew/homebrew-core") }
      it { should be_directory }
    end

    describe 'homebrew/homebrew-cask' do
      let(:subject) { file("#{brew_repository}/Library/Taps/homebrew/homebrew-cask") }
      it { should be_directory }
    end

    describe 'gitlab/homebrew-shared-runners' do
      let(:subject) { file("#{brew_repository}/Library/Taps/gitlab/homebrew-shared-runners") }
      it { should be_directory }
    end
  end

  # Built-in tests should pass for each installed formula
  xcontext 'built-in formula tests' do
    brew_list = 'brew list --formula -1'
    run_on_target(brew_list).strip.each_line do |formula|
      formula = formula.strip
      next if formula == 'six' # this is a dependency of awscli and it has test dependencies which we don't install via homebrew
      next if formula == 'fvm' # the leoafarias/fvm formula has tests that explicitely fail
      next if formula == 'carthage' # this test is unreliable https://github.com/Homebrew/homebrew-core/issues/3201, also tested in toolchain_spec.rb

      describe formula do
        it 'should pass' do
          cmd = command("brew test #{formula}")
          expect(cmd).to be_a_successful_cmd
        end
      end
    end
  end
end
