# frozen_string_literal: true

require 'spec_helper'

xcontext 'Xcode' do
  describe 'xcodes version manager' do
    it 'is installed and globally active' do
      cmd = command('xcodes installed')
      expect(cmd).to be_a_successful_cmd
    end

    it 'returns the expected installed versions' do
      cmd = command('xcodes installed')
      versions = cmd.stdout.split("\n").map{|v| v.split(' ').first}

      expect(versions).to include("13.4.1")
      expect(versions).to include("14.3.1")
      expect(versions).to include("15.0")
    end

    it 'defaults to version 14.3.1' do
      cmd = command('xcodebuild -version')
      expect(cmd.stdout).to start_with('Xcode 14.3.1')
    end

    it 'allows the version to be changed' do
      cmd = command('sudo xcodes select 13.4.1')
      expect(cmd.stdout).to eq("Selected /Applications/Xcode-13.4.1.app/Contents/Developer\n\n")
      cmd = command('xcodebuild -version')
      expect(cmd.stdout).to start_with('Xcode 13.4.1')
      cmd = command('sudo xcodes select 14.3.1')
      expect(cmd.stdout).to eq("Selected /Applications/Xcode-14.3.1.app/Contents/Developer\n\n")
      cmd = command('xcodebuild -version')
      expect(cmd.stdout).to start_with('Xcode 14.3.1')
    end
  end
end